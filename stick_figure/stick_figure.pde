void setup(){
 size(900,500); 
}

// Just define some variables that define the stick figure

float headX=450;
float headY=100;
float hipX;
float hipY;

float torsolength=200;

float leftArmX;
float leftArmY;
float rightArmX;
float rightArmY;
float neckLength = 60;
float armLength = 100;
float leftArmAngle = radians(30);
float rightArmAngle = radians(30);

float leftKneeX;
float leftKneeY;
float rightKneeX;
float rightKneeY;
float leftAnkleX;
float leftAnkleY;
float rightAnkleX;
float rightAnkleY;
float kneeToHip = 100;
float kneeToAnkle = 100;
float hipAngle = radians(30);
float kneeAngle = radians(60);
 
void draw(){
 background(170);
 
 // figure out where all the joints are and then we will draw
 
 hipX=headX;
 hipY=headY+torsolength;
 
 leftArmX = headX - armLength*cos(leftArmAngle);
 leftArmY = headY + neckLength - armLength*sin(leftArmAngle);

 rightArmX = headX + armLength*cos(rightArmAngle);
 rightArmY = headY + neckLength - armLength*sin(rightArmAngle);
 
 leftKneeX = hipX - kneeToHip*cos(hipAngle);
 leftKneeY = hipY + kneeToHip*sin(hipAngle);
 
 leftAnkleX = leftKneeX - kneeToAnkle*cos(kneeAngle);
 leftAnkleY = leftKneeY + kneeToAnkle*sin(kneeAngle);
 
 rightKneeX = hipX + kneeToHip*cos(hipAngle);
 rightKneeY = hipY + kneeToHip*sin(hipAngle);
 
 rightAnkleX = rightKneeX + kneeToAnkle*cos(kneeAngle);
 rightAnkleY = rightKneeY + kneeToAnkle*sin(kneeAngle);
 
 // draw the torso
 line (headX,headY,hipX,hipY);
 
 // draw the arms
 line (headX,headY+neckLength,leftArmX,leftArmY);
 line (headX,headY+neckLength,rightArmX,rightArmY);
 
 // draw the head
 ellipse(headX,headY,80,80);
 
 // draw the legs of the stick figure
 line (hipX,hipY,leftKneeX,leftKneeY);
 line (leftKneeX,leftKneeY,leftAnkleX,leftAnkleY);
 
 line (hipX,hipY,rightKneeX,rightKneeY);
 line (rightKneeX,rightKneeY,rightAnkleX,rightAnkleY);
}
